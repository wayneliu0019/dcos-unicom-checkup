# dcos-unicom-checkup
ansible playbook for Unicom production cluster 76 checkup process

## checkup process and commands
`ansible-playbook basicinfo.yml`
1. cluster node info are stored under workdir/all-node-info.txt
2. check all cluster node meet DC/OS requirements or not

`ansible-playbook checkup.yml`
* master component
  1. customized config file location
  2. customized config items
  3. component healthy status

  above infos are stored under workdir/results/masters/

* check agent component

* dcos-net component
   1. consistence of customized dns servers
   2. external domain name resolve result
   3. whether dcos-net crashed in latest 7 days

`ansible all -b -m shell "systemctl status dcos-net | grep Active" >./dcos-net-status.txt`

`ansible all -b -m shell "systemctl status dcos-net | grep Memory" >./dcos-net-memory.txt`


## DC/OS bundle
generate DC/OS bundle by interval, such as bi-weekly
```
dcos node diagnostics create all
dcos node diagnostics --status
(wait for progress to reach 100%)
dcos node diagnostics download <bundle_name>
```

## dcos-net run book
generate problematic node's dcos-net detail infos with `files/dcos-net/dcos-net-runbook-unicom.sh`

## analysis erl_crash.dump
parse erlang dump file with `files/dcos-net/erl_crashdump_analyzer.sh`

## Useful tools
`tools/generate_inventory.sh`

the script is used to generate inventory file for ansible.

**PLEASE NOTE**
1. `dcos` command is used to retrieve all node ips in this script, please make sure dcos command is configured before run this script
2. the generated inventory file name is `inventory_tmp`


`files/dcos-net/dcos-net-runbook-unicom.sh`

the script is used to generate dcos-net infos on speicfic node

`files/dcos-net/erl_crashdump_analyzer.sh`

the script is used too parse the dcos-net's dump file