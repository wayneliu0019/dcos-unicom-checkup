#!/bin/bash

# the script is used to generate inventory file for ansible, the inventory template is like below:

#[bootstraps]
#10.161.1.76

#[masters]

#[agents_private]

#[agents_public]

#[agents:children]
#agents_private
#agents_public

#[common:children]
#masters
#agents_public
#agents_private

# PLEASE NOTE:
# 1. "dcos" command is used to retrieve all node ips in this script, please make sure dcos command is configured before run this script
# 2. the generated inventory file name is "inventory_tmp",


echo "get all node info to node.json"
dcos node --json >node.json

echo "get all masters ips to masters.txt"
cat node.json| jq --raw-output '.[] | select(.host == "master.mesos.") | .ip' > masters.txt

echo "get all public agent  ips to public.txt"
cat node.json |  jq --raw-output '.[] | select(.attributes.public_ip == "true") | .hostname' > public.txt

echo "get all private agent ips to agent.txt"
dcos node | awk '{print $2}' | sed '1d' > ips.txt
cp ips.txt agent.txt

while read master
do
sed -i "s/${master}//g" agent.txt
done < masters.txt


while read public
do
sed -i "s/${public}//g" agent.txt
done < public.txt


echo "generate inventory file based on current cluster node"

tee inventory_tmp <<-"EOF"
[bootstraps]
10.161.1.76

[masters]

[agents_private]

[agents_public]

[agents:children]
agents_private
agents_public

[common:children]
masters
agents_public
agents_private
EOF


echo "insert master ips to inventory file"
sed -i '/\[masters\]/r masters.txt' inventory_tmp

echo "insert public ips to inventory file"
sed -i '/\[agents_public\]/r public.txt' inventory_tmp

echo "insert agent ips to inventory file"
sed -i '/\[agents_private\]/r agent.txt' inventory_tmp
